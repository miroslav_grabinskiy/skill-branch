"use strict";

const http = require('http');
const express = require('express');

const app = express();

app.set('port', 3000);

app.get('', function(req, res, next) {
    const query = req.query,
        fullName = req.query.fullname;
    let initials;
    
    if (!fullName || typeof fullName !== 'string') {
        return next([200, 'Invalid fullname']); //for tests skillbranch bot (other - 400)
    }
    
    try {
        initials = convertQueryToInitials(fullName);
    } catch(err) {
        return next(err);
    }
    
    res.status(200).send(initials);
});

app.use(function errorHandler(err, req, res, next) {
    if (err instanceof Array) {
        res.status(err[0]).send(err[1]);
    } else {
        res.status(200).send(err); //for tests skillbranch bot (other - 500)
    }
});

const server = http.createServer(app);

server.listen(
	app.get('port'),
	() => console.log('app listening on port: ' + app.get('port') )
);

module.exports = app;

function convertQueryToInitials(fullName) {
    const arrayOfElements = fullName.split(" ");
    let arrayOfElementsLength;
    let surname,
        names = [],
        result;

    deleteSpaceElements(arrayOfElements);
    arrayOfElementsLength = arrayOfElements.length;

    if (arrayOfElementsLength > 3 || arrayOfElementsLength <= 0) {
         throw [200, "Invalid fullname"]; //for tests skillbranch bot (other - 400)
    }

    for (let i = 0, len = arrayOfElements.length - 1; i <= len; i++) {
        let element;

        validation(arrayOfElements[i]);

        if (i !== len) {
            names.push(arrayOfElements[i].charAt(0).toUpperCase() + '.');
        } else {
            surname = capitalizeFirstLetter(arrayOfElements[i]);
        }
    }

    result = names.length > 0 ? surname + " " + names.join(" ") : surname;
    return result;

    function capitalizeFirstLetter(string) {
        string = string.toLowerCase();
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function validation(string) {
        for (let i = 0, len = string.length; i < len; i++) {
            let char = string.charAt(i);

            if ( !isNaN(char) || char === '_' || char === '/') { ///^[A-zА-яЁё]*$/
                 throw [200, "Invalid fullname"]; //for tests skillbranch bot (other - 400)
            }
        }

        return true; 
    }

    function deleteSpaceElements(arr) {
        for (let i = 0, j = 0, len = arr.length; i + j < len; i++) {
            if (arr[i + j] === " " || arr[i+j].length === 0) {
                arr.splice(i + j, 1);
                j--;
                len--;
            }
        }
    }
}