"use strict";

const http = require('http');
const express = require('express');

const app = express();

app.set('port', 3000);

app.get('', function(req, res) {
    const query = req.query;
    query.a = Number(query.a) || 0; 
    query.b = Number(query.b) || 0;
    
    res.status(200).send( (query.a + query.b).toString() );
});

const server = http.createServer(app);

server.listen(
	app.get('port'),
	() => console.log('app listening on port: ' + app.get('port') )
);


module.exports = app;