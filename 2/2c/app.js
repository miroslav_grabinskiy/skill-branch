"use strict";

const http = require('http');
const express = require('express');

const app = express();
const cors = require('cors');

app.set('port', 3054);
app.use(cors());

app.get('', function(req, res, next) {
    const query = req.query,
        username = req.query.username;
    let name;

    if (!username || typeof username !== 'string') {
        return next([200, 'Invalid username']); //for tests skillbranch bot (other - 400)
    }
    
    try {
        name = convertQueryToUserName(username);
    } catch(err) {
        return next(err);
    }
    
    res.status(200).send(name);
});

app.use(function errorHandler(err, req, res, next) {
    if (err instanceof Array) {
        res.status(err[0]).send(err[1]);
    } else {
        res.status(200).send(err); //for tests skillbranch bot (other - 500)
    }
});

const server = http.createServer(app);

server.listen(
	app.get('port'),
	() => console.log('app listening on port: ' + app.get('port') )
);

module.exports = app;


function convertQueryToUserName(username) {
    let result;
    
    let domenPos = username.indexOf('.com/');


    if ( domenPos >= 0) {
        result = username.substring(domenPos + 5, username.length);
        result = result.split('/');
        result = result[0];
    } else {
        result = username.split('/');
        result = result[result.length - 1];
    }

    result = result.split('?');
    result = result[0];

    if (result.charAt(0) !== '@') {
        result = '@' + result;
    }

    return result;
}